# README #

Proyecto Java EE con para encuesta de Alumnos, Padres y Compañeros de trabajo
### Tecnologías necesarias? ###

* Java 1.8.0_202
* MySQL 8.0.19
* MySQL Workbench 8.0
* Apache Netbeans IDE 12.0
* Apache Tomcat 5.5.56


### Librerias ###

* gson-2.8.5.jar
* mysql-connector-java-8.0.20.jar


### ENDPOINTS ###

endpoint tipos no ocupa parametros regresa los tipos alumno,maestro,compañero
http://localhost:8080/Encuesta/Tipos

endpoint entidades no ocupa parametros regresa las entidades federativas
http://localhost:8080/Encuesta/Entidades


endpoint plantel ocupa parametro entidad con un valor de 1  a 32
http://localhost:8080/Encuesta/Plantel?entidad=30


endpoint salvar todo el formulario, revisar paquete datos la clase
//ServletControladorGuardar
http://localhost:8080/Encuesta/ServletControladoGuardar
